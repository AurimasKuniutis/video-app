import React from "react";
import Select from "react-select";
import quantityOptions from "./quantityValues";

const DurationInput = (props: any) => {
  return (
    <>
      <label htmlFor="qantity">Select how many videos to play:</label>
      <Select
        id="qantity"
        options={quantityOptions}
        value={props.quantityValue}
        onChange={props.quantityChange}
      />
    </>
  );
};
export default DurationInput;
