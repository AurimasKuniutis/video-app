const quantityOptions: {
    value: number;
    label: string;
}[] = [];

for(let i = 1; i <= 10; i++){
    quantityOptions[i] = {
        value: i,
        label: i.toString()
    }
}
export default quantityOptions;