import React from 'react'

const Caption = (props: any) => {
    return (
        <h5 className='caption'>{props.caption}</h5>
    )
}
export default Caption;