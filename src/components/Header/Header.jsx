import React from 'react';

const Header = () => {
    return (
        <header>
            <h1>Video App</h1>
        </header>
    )
}
export default Header;