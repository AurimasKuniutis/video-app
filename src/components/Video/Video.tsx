import React from 'react';

const Video = (props: any) => {
    return(
        <video className='video' autoPlay muted src={props.link} ></video>
    )
}
export default Video;