import React from "react";
import QantitySelect from "../QantitySelect";
import DurationSelect from "../DurationSelect";
import SearchBar from "../SearchBar";

const Controls = (props: any) => {
  return (
    <div>
      <SearchBar searchChange={props.searchChange}/>
      <QantitySelect
        quantityValue={props.quantityValue}
        quantityChange={props.quantityChange}
      />
      <DurationSelect
        durationValue={props.durationValue}
        durationChange={props.durationChange}
      />
    </div>
  );
};
export default Controls;
