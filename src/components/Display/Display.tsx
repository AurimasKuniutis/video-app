import React from "react";
import Spinner from "react-bootstrap/Spinner";
import Caption from "../Caption";
import Video from "../Video";

const Display = (props: any) => {
  return (
    <div className="display">
      <Video link={props.link} />
      {props.isLoading ? 
            <Spinner className="spinner" animation="border" role="status" variant="primary">
            <span className="sr-only">Loading...</span>
          </Spinner> : null
      }
      <Caption caption={props.caption} />
    </div>
  );
};
export default Display;
