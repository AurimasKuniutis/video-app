import React from "react";

const SearchBar = (props: any) => {
  return (
    <>
      <label htmlFor="search">Search:</label>
      <input id="search"onChange={props.searchChange} type="text"></input>
    </>
  );
};
export default SearchBar;
