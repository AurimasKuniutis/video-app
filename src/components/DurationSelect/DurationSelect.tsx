import React from "react";
import Select from "react-select";
import durationOptions from "./durationValues";

const DurationInput = (props: any) => {
  return (
    <>
      <label htmlFor="duration">Select duration:</label>
      <Select
        id="duration"
        options={durationOptions}
        value={props.durationValue}
        onChange={props.durationChange}
      />
    </>
  );
};
export default DurationInput;
