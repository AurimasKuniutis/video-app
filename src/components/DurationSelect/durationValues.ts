const durationOptions = [
    { value: 10,label: '10s' },
    { value: 20,label: '20s' },
    { value: 30,label: '30s' },
];
export default durationOptions;