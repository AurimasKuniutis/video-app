import React from "react";
import Header from "./components/Header";
import Controls from "./components/Controls";
import Display from "./components/Display";
import axios from "axios";
import { debounce } from "ts-debounce";
import "./App.css";
import 'bootstrap/dist/css/bootstrap.min.css';

type SelectValue = {
  value: number
  label: string
}

class App extends React.Component {
  state = {
    selectedDuration: { value: 5, label: "5s" },
    selectedQantity: { value: 1, label: "1" },
    topic: "",
    videos: [],
    currentVideo: {
      user: { name: "" },
      video_files: [{ link: "" }],
    },
    isLoading: false,
  };

  timeout: any;
  playVideos = (arr: any[], delay: number) => {
    const {selectedDuration} = this.state;
    let i = 0;
    const setVideo = (): void => {
      const label =this.getDuration(arr[i]).toString()+"s";
      const newDuration = {value: selectedDuration.value, label:label};
      this.setState({selectedDuration: newDuration})
      this.setState({ currentVideo: arr[i] });
      console.log("i: " + i)
      i++;
      if (i < arr.length) {
        this.timeout = setTimeout(setVideo, this.getDuration(arr[i]) * 1000);
      } else {
        i = 0;
        setVideo();
      }
    };
    setVideo();
  };

  stopVideos = () => {
    clearTimeout(this.timeout);
  }

  getDuration = (currentVideo: any): number => {
    const {selectedDuration} = this.state;
    let duration = selectedDuration.value;
    if(currentVideo!==undefined){
      duration = (currentVideo.duration < selectedDuration.value)
      ? currentVideo.duration
      : selectedDuration.value;
    }
    return duration;
  }

  apiCall = (title: string, quantity: number) => {
    const { selectedDuration } = this.state;
    const searchQuery = title + "&orientation=landscape&per_page=" + quantity;
    this.setState({isLoading: true})
    axios.get(searchQuery)
      .then((response) => {
        this.setState({ videos: response.data.videos });
        this.setState({isLoading: false})
        this.playVideos(response.data.videos, selectedDuration.value);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  handleSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.stopVideos();
    this.setState({ topic: event.target.value });
    this.apiCall(event.target.value, this.state.selectedQantity.value);
  };
  debouncedSearchChange = debounce(this.handleSearchChange, 2000);

  handleDurationChange = (selectedValue: SelectValue) => {
    const {videos, topic, selectedDuration} = this.state;
    this.setState({ selectedDuration: selectedValue});
    this.stopVideos();
    if(topic !== ""){
      console.log(videos)
      this.playVideos(videos, selectedDuration.value * 1000);
    }
  };
  handleQantityChange = (selectedValue: SelectValue) => {
    this.stopVideos();
    const {topic, selectedQantity} = this.state;
    this.setState({ selectedQantity: selectedValue});

    if(topic !== "") {
      this.apiCall(topic, selectedQantity.value);
    };
  };
  render() {
    const { selectedDuration, selectedQantity, currentVideo } = this.state;
    return (
      <div className="App">
        <Header />
        <div className="layout">
          <Controls
            searchChange={this.debouncedSearchChange}
            durationValue={selectedDuration}
            durationChange={this.handleDurationChange}
            quantityValue={selectedQantity}
            quantityChange={this.handleQantityChange}
          />
          <Display
            duration={selectedDuration}
            link={currentVideo !== undefined ? currentVideo.video_files[0].link : ""}
            caption={currentVideo ? currentVideo.user.name : "Try to search for some videos"}
            isLoading={this.state.isLoading}
          />
        </div>
      </div>
    );
  }
}

export default App;
